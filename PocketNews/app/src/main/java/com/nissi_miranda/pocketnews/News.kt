package com.nissi_miranda.pocketnews

import android.content.Context
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout

data class News(var sourceId: String, var title: String, var description: String, var newsUrl: String, var newsImageUrl: String,
                var sourceId_n2: String, var title_n2: String, var description_n2: String, var newsUrl_n2: String, var newsImageUrl_n2: String)

class CellType()
{
    companion object {
        val SINGLE_NEWS_CELL: Int = 0
        val SINGLE_NEWS_FULLSCREEN_CELL: Int = 1
        val DOUBLE_NEWS_CELL: Int = 2
    }
}

open class NewsCell(itemView: View) : RecyclerView.ViewHolder(itemView)
{
    val newsInfo = itemView.findViewById(R.id.newsInfo) as LinearLayout
    val newsTitle = itemView.findViewById(R.id.newsTitle) as AppCompatTextView
    val newsDesc = itemView.findViewById(R.id.newsDesc) as AppCompatTextView
    val shareButton = itemView.findViewById(R.id.shareButton) as ImageView
}

class SingleNewsCell(itemView: View) : NewsCell(itemView)
{
    val newsCellFrame = itemView.findViewById(R.id.newsCellFrame) as FrameLayout
    val container = itemView.findViewById(R.id.container) as FrameLayout
    val newsImage = itemView.findViewById(R.id.newsImage) as ImageView
}

class SingleNewsFullscreenCell(itemView: View, val context: Context) : NewsCell(itemView)
{
    val newsCellFrame = itemView.findViewById(R.id.newsCellFrame) as FrameLayout
    val newsImage = itemView.findViewById(R.id.newsImage) as ImageView
    val sourceView = itemView.findViewById(R.id.sourceView) as LinearLayout
    val sourceName = itemView.findViewById(R.id.sourceName) as AppCompatTextView
    val sourceImage = itemView.findViewById(R.id.sourceImage) as ImageView

    init
    {
        if (context is MainActivity)
        {
            sourceView.visibility = View.VISIBLE
            sourceView.isClickable = true
        }
        else
        {
            sourceView.visibility = View.GONE
            sourceView.isClickable = false
        }
    }
}

class DoubleNewsCell(itemView: View) : NewsCell(itemView)
{
    val newsCellFrame = itemView.findViewById(R.id.newsCellFrame) as LinearLayout
    val logoImage = itemView.findViewById(R.id.logoImage) as ImageView
    val logoImage_n2 = itemView.findViewById(R.id.logoImage_n2) as ImageView
    val newsInfo_n2 = itemView.findViewById(R.id.newsInfo_n2) as LinearLayout
    val newsTitle_n2 = itemView.findViewById(R.id.newsTitle_n2) as AppCompatTextView
    val newsDesc_n2 = itemView.findViewById(R.id.newsDesc_n2) as AppCompatTextView
    val shareButton_n2 = itemView.findViewById(R.id.shareButton_n2) as ImageView
}