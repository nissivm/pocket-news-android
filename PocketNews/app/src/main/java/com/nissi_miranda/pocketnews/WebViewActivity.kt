package com.nissi_miranda.pocketnews

import android.os.Bundle
import android.annotation.TargetApi
import android.os.Build
import android.util.Log
import android.annotation.SuppressLint
import android.view.MenuItem
import android.webkit.*
import android.view.View
import android.widget.PopupMenu
import kotlinx.android.synthetic.main.web_view_activity.*

class WebViewActivity : BaseActivity(), PopupMenu.OnMenuItemClickListener
{
    private lateinit var newsUrl: String
    private lateinit var popup: PopupMenu
    private var showingPopup = false

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.web_view_activity)

        Utilities.topActivity = "WebViewActivity"

        newsUrl = intent.getStringExtra("stringUrlToOpen")

        urlTextView.text = formatStringUrl(newsUrl)

        closeButton.setOnClickListener {
            finish()
            overridePendingTransition(R.anim.idle_animation, R.anim.slide_to_bottom)
        }

        optionsButton.setOnClickListener { showMenu() }

        webView.webViewClient = LocalWebClient()
        webView.webChromeClient = LocalChromeClient()
        webView.settings.javaScriptEnabled = true
        webView.loadUrl(newsUrl)
    }

    override fun onBackPressed()
    {
        if (showingPopup)
        {
            popup.dismiss()
            showingPopup = false
            return
        }

        if (webView.canGoBack())
        {
            Log.i("WebViewActivity", "Can go back")
            webView.goBack()
        }
        else
        {
            Log.i("WebViewActivity", "Can NOT go back")
            super.onBackPressed()
        }
    }

    private fun showMenu()
    {
        showingPopup = true

        popup = PopupMenu(this, optionsButton)
        popup.setOnMenuItemClickListener(this)
        popup.inflate(R.menu.web_view_activity_menu)
        popup.show()
    }

    override fun onMenuItemClick(item: MenuItem) : Boolean
    {
        showingPopup = false

        if (item.itemId == R.id.reload)
        {
            webView.reload()
            return true
        }
        else if (item.itemId == R.id.share)
        {
            Utilities.shareNewsUrl(this, newsUrl)
            return true
        }

        return false
    }

    private fun formatStringUrl(stringUrl: String) : String
    {
        var uiUrl = stringUrl

        if (stringUrl.startsWith("https://", true))
        {
            uiUrl = uiUrl.substring(8)
        }
        else if (stringUrl.startsWith("http://", true))
        {
            uiUrl = uiUrl.substring(7)
        }

        val idx = uiUrl.indexOf("/")

        return if (idx != -1) uiUrl.substring(0, idx) else uiUrl
    }

    private inner class LocalWebClient : WebViewClient()
    {
//        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?)
//        {
//            // Notify the host application that a page has started loading.
//            super.onPageStarted(view, url, favicon)
//        }

        override fun onPageFinished(view: WebView?, url: String?)
        {
            // Notify the host application that a page has finished loading.
            if (url != null) { urlTextView.text = formatStringUrl(url) }
            super.onPageFinished(view, url)
        }

        @Suppress("OverridingDeprecatedMember")
        override fun shouldOverrideUrlLoading(view: WebView, url: String) : Boolean
        {
            return false
        }

        //@TargetApi(Build.VERSION_CODES.N)
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean
        {
            return false
        }
    }

    private inner class LocalChromeClient : WebChromeClient()
    {
        override fun onProgressChanged(view: WebView, progress: Int) {

            progressBar.progress = progress

            if (progress == 100)
            {
                progressBar.visibility = View.GONE
            }
            else
            {
                progressBar.visibility = View.VISIBLE
            }
        }
    }
}