package com.nissi_miranda.pocketnews

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONArray
import java.util.*

class SingleSourceNewsActivity : BaseActivity()
{
    lateinit private var sourceImage: ImageView
    lateinit private var sourceName: AppCompatTextView
    lateinit private var recyclerView: RecyclerView
    lateinit private var swipeRefresh: SwipeRefreshLayout

    private var timer: Timer? = null
    private var timerTask: TimerTask? = null
    private var topHeadlinesJsonObjectRequest: JsonObjectRequest? = null
    private var everythingJsonObjectRequest: JsonObjectRequest? = null

    lateinit var layoutManager: GridLayoutManager
    private var scrollPosition = 0

    private var tempSingleSourceNews = ArrayList<News>()

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        Utilities.topActivity = "SingleSourceNewsActivity"
        performLayoutSetup()

        swipeRefresh.isRefreshing = true
        recyclerView.visibility = View.INVISIBLE
        organizeNews(Utilities.newsDic[Utilities.openedSourceId]!!, true)

        timer = Timer()

        timerTask = object: TimerTask() {

            override fun run() {

                timer = null
                timerTask = null
                startFetchingRestOfSingleSourceNews()
            }
        }

        timer!!.schedule(timerTask!!, 2000)
    }

    override fun onResume()
    {
        super.onResume()

        if (Utilities.topActivity != "SingleSourceNewsActivity")
        {
            Utilities.topActivity = "SingleSourceNewsActivity"
            performLayoutSetup()
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration?)
    {
        super.onConfigurationChanged(newConfig)
        performLayoutSetup()
    }

    override fun onStop()
    {
        super.onStop()

        if (topHeadlinesJsonObjectRequest != null)
        {
            App.instance().volleyRequestQueue()!!.cancelAll(App.TAG)
            topHeadlinesJsonObjectRequest = null
        }

        if (everythingJsonObjectRequest != null)
        {
            App.instance().volleyRequestQueue()!!.cancelAll(App.TAG)
            everythingJsonObjectRequest = null
        }

        Utilities.fetchingTopHeadlines = false
        Utilities.fetchingRestOfSingleSourceNews = false
        swipeRefresh.isRefreshing = false
    }

    //----------------------------------------------------------------------------------------//
    // Start fetching top headlines
    //----------------------------------------------------------------------------------------//

    private fun startFetchingTopHeadlines()
    {
        Utilities.fetchingTopHeadlines = true
        swipeRefresh.isRefreshing = true

        val context = this as Context

        timer = Timer()

        timerTask = object: TimerTask() {

            override fun run() {

                timer = null
                timerTask = null
                topHeadlinesJsonObjectRequest = ServerAccess.fetchTopHeadlinesForSingleSource(context)
            }
        }

        timer!!.schedule(timerTask!!, 2000)
    }

    //----------------------------------------------------------------------------------------//
    // Start fetching rest of single source news
    //----------------------------------------------------------------------------------------//

    private fun startFetchingRestOfSingleSourceNews()
    {
        Utilities.fetchingRestOfSingleSourceNews = true
        everythingJsonObjectRequest = ServerAccess.fetchRestOfSingleSourceNews(this)
    }

    //----------------------------------------------------------------------------------------//
    // Call back when finishing fetching top headlines
    //----------------------------------------------------------------------------------------//

    fun finishedFetchingTopHeadlines(articles: JSONArray)
    {
        Utilities.fetchingTopHeadlines = false
        topHeadlinesJsonObjectRequest = null
        setupFetchedNews(articles, true)
        startFetchingRestOfSingleSourceNews()
    }

    //----------------------------------------------------------------------------------------//
    // Call back when finishing fetching rest of single source news
    //----------------------------------------------------------------------------------------//

    fun finishedFetchingRestOfSingleSourceNews(articles: JSONArray)
    {
        Utilities.fetchingRestOfSingleSourceNews = false
        everythingJsonObjectRequest = null
        setupFetchedNews(articles, false)
        swipeRefresh.isRefreshing = false
    }

    //----------------------------------------------------------------------------------------//
    // Call back when finishing fetching news for a single source
    //----------------------------------------------------------------------------------------//

    fun finishedFetchingNewsWithError()
    {
        if (Utilities.fetchingTopHeadlines)
        {
            Utilities.fetchingTopHeadlines = false
            topHeadlinesJsonObjectRequest = null
            startFetchingRestOfSingleSourceNews()
            return
        }

        if (Utilities.fetchingRestOfSingleSourceNews)
        {
            Utilities.fetchingRestOfSingleSourceNews = false
            everythingJsonObjectRequest = null
            setupFetchedNews(null, false)
            swipeRefresh.isRefreshing = false
        }
    }

    //----------------------------------------------------------------------------------------//
    // Setup fetched top headlines
    //----------------------------------------------------------------------------------------//

    private fun setupFetchedNews(articles: JSONArray?, isTopHeadlines: Boolean)
    {
        if (articles == null)
        {
            if (tempSingleSourceNews.size > 0)
            {
                Utilities.singleSourceNews = tempSingleSourceNews
                recyclerView.adapter.notifyDataSetChanged()
                tempSingleSourceNews = ArrayList<News>()
            }

            return
        }

        val newsArray = ArrayList<News>()

        for (i in 0..(articles.length() - 1))
        {
            val article = articles.getJSONObject(i)

            if (Utilities.isValidNews(article))
            {
                val sourceId = article.getJSONObject("source").getString("id")
                var title = Utilities.getStringValue(article, "title")
                var description = Utilities.getStringValue(article, "description")
                val url = Utilities.getStringValue(article, "url")
                val urlToImage = Utilities.getArticleImageUrl(article)

                title = Utilities.getParsedHtmlText(title)

                if (description != "")
                {
                    description = Utilities.getParsedHtmlText(description)

                    if (!description.endsWith(".", true))
                    {
                        description += "."
                    }
                }

                var found = false

                if (tempSingleSourceNews.size > 0)
                {
                    for (n in tempSingleSourceNews)
                    {
                        if ((n.newsUrl == url) || (n.title == title) || (n.description == description) || (n.newsImageUrl == urlToImage))
                        {
                            found = true
                            break
                        }
                    }
                }

                if (!found)
                {
                    newsArray.add(News(sourceId, title, description, url, urlToImage, "", "", "", "", ""))
                }
            }
        }

        if (newsArray.size > 0) { organizeNews(newsArray, isTopHeadlines) }

        if ((!isTopHeadlines) && (tempSingleSourceNews.size > 0))
        {
            Utilities.singleSourceNews = tempSingleSourceNews
            recyclerView.adapter.notifyDataSetChanged()
            recyclerView.visibility = View.VISIBLE
            tempSingleSourceNews = ArrayList<News>()
        }
    }

    //----------------------------------------------------------------------------------------//
    // Organize news by photos / no photos
    //----------------------------------------------------------------------------------------//

    private fun organizeNews(newsArray: ArrayList<News>, isTopHeadlines: Boolean)
    {
        val articlesWithPhoto = ArrayList<News>()
        val articlesWithoutPhoto = ArrayList<News>()
        val doubleNews = ArrayList<News>()

        for (i in 0..(newsArray.size - 1))
        {
            val news = newsArray[i]

            if (news.newsImageUrl != "") { articlesWithPhoto.add(news) }
            else { articlesWithoutPhoto.add(news) }
        }

        if (articlesWithoutPhoto.size % 2 != 0)
        {
            articlesWithoutPhoto.removeAt(articlesWithoutPhoto.size - 1)
        }

        while (articlesWithoutPhoto.size > 0)
        {
            val n = articlesWithoutPhoto[0]
            articlesWithoutPhoto.removeAt(0)
            val n_ = articlesWithoutPhoto[0]
            articlesWithoutPhoto.removeAt(0)

            val dNews = News(n.sourceId, n.title, n.description, n.newsUrl, n.newsImageUrl,
                    n_.sourceId, n_.title, n_.description, n_.newsUrl, n_.newsImageUrl)
            doubleNews.add(dNews)
        }

        if (articlesWithPhoto.size > 0)
        {
            for (i in 0..(articlesWithPhoto.size - 1))
            {
                val newsWithPhoto = articlesWithPhoto[i]
                tempSingleSourceNews.add(newsWithPhoto)

                if (doubleNews.size > 0)
                {
                    val dNews = doubleNews[0]
                    tempSingleSourceNews.add(dNews)
                    doubleNews.removeAt(0)
                }
            }

            while (doubleNews.size > 0)
            {
                val dNews = doubleNews[0]
                tempSingleSourceNews.add(dNews)
                doubleNews.removeAt(0)
            }
        }
        else
        {
            tempSingleSourceNews.addAll(doubleNews)
        }

        if (!isTopHeadlines)
        {
            if (tempSingleSourceNews.size % 2 != 0)
            {
                tempSingleSourceNews.removeAt(tempSingleSourceNews.size - 1)
            }
        }
    }

    //----------------------------------------------------------------------------------------//
    // Perform layout setup
    //----------------------------------------------------------------------------------------//

    private fun performLayoutSetup()
    {
        val isTablet = resources.getBoolean(com.nissi_miranda.pocketnews.R.bool.isTablet)
        val isPortrait = resources.getBoolean(com.nissi_miranda.pocketnews.R.bool.isPortrait)
        val spanCount = if (isTablet) 4 else 2
        val orientation = if (isPortrait) GridLayoutManager.VERTICAL else GridLayoutManager.HORIZONTAL

        if (isPortrait) { setContentView(R.layout.single_source_news_activity_portrait) }
        else { setContentView(R.layout.single_source_news_activity_landscape) }

        sourceImage = findViewById(R.id.sourceImage)
        sourceName = findViewById(R.id.sourceName)
        swipeRefresh = findViewById(R.id.swipeRefresh)
        recyclerView = findViewById(R.id.recyclerView)

        sourceName.text = Utilities.getTitleForSourceId(Utilities.openedSourceId)
        val drawableId = Utilities.getDrawableForSourceId(Utilities.openedSourceId)
        val drawable = ResourcesCompat.getDrawable(resources, drawableId, null)
        sourceImage.setImageDrawable(drawable)

        swipeRefresh.setOnRefreshListener { startFetchingTopHeadlines() }
        swipeRefresh.isRefreshing = Utilities.fetchingTopHeadlines

        recyclerView.addOnScrollListener(ScrollListener())
        recyclerView.adapter = NewsRecyclerViewAdapter(this)
        layoutManager = GridLayoutManager(this, spanCount, orientation, false)
        layoutManager.spanSizeLookup = CustomSpanSizeLookup()
        recyclerView.layoutManager = layoutManager
        layoutManager.scrollToPosition(scrollPosition)
    }

    private inner class CustomSpanSizeLookup : GridLayoutManager.SpanSizeLookup()
    {
        override fun getSpanSize(position: Int): Int
        {
            return 2
        }
    }

    private inner class ScrollListener : RecyclerView.OnScrollListener()
    {
        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int)
        {
            super.onScrolled(recyclerView, dx, dy)
            scrollPosition = layoutManager.findFirstVisibleItemPosition()
        }
    }
}