package com.nissi_miranda.pocketnews

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.NoConnectionError
import com.android.volley.TimeoutError
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONException

class ServerAccess
{
    companion object
    {
        fun fetchTopHeadlines(context: Context, round: Int) : JsonObjectRequest
        {
            val sourcesIds = Utilities.getSourcesIdsToRequest(round)
            val url = "https://newsapi.org/v2/top-headlines?sources=$sourcesIds&apiKey=${Constants.newsAPIKey}"

            val jsObjRequest = JsonObjectRequest(Request.Method.GET, url, null,
                    object: Response.Listener<JSONObject> {

                        override fun onResponse(response: JSONObject) {

                            try
                            {
                                if (response.getString("status") != "ok")
                                {
                                    Log.e("ServerAccess", "fetchTopHeadlines error -> status != ok")

                                    var errMsg = context.resources.getString(com.nissi_miranda.pocketnews.R.string.general_error_message)
                                    (context as MainActivity).finishedFetchingTopHeadlinesWithErrors(errMsg, round)

                                    if ((round == 1) && (Utilities.mixedSourcesNews.isNotEmpty()))
                                    {
                                        errMsg = context.resources.getString(com.nissi_miranda.pocketnews.R.string.update_news_error_message)
                                        Toast.makeText(context, errMsg, Toast.LENGTH_LONG).show()
                                    }

                                    return
                                }

                                val articles = response.getJSONArray("articles") as JSONArray

                                if (articles.length() == 0)
                                {
                                    Log.e("ServerAccess", "fetchTopHeadlines error -> number of articles == 0")

                                    var errMsg = context.resources.getString(com.nissi_miranda.pocketnews.R.string.general_error_message)
                                    (context as MainActivity).finishedFetchingTopHeadlinesWithErrors(errMsg, round)

                                    if ((round == 1) && (Utilities.mixedSourcesNews.isNotEmpty()))
                                    {
                                        errMsg = context.resources.getString(com.nissi_miranda.pocketnews.R.string.update_news_error_message)
                                        Toast.makeText(context, errMsg, Toast.LENGTH_LONG).show()
                                    }

                                    return
                                }

                                (context as MainActivity).finishedFetchingTopHeadlines(articles, round)
                            }
                            catch (e: JSONException)
                            {
                                Log.e("ServerAccess", "fetchTopHeadlines exception -> ${e.localizedMessage}")

                                var errMsg = context.resources.getString(com.nissi_miranda.pocketnews.R.string.general_error_message)
                                (context as MainActivity).finishedFetchingTopHeadlinesWithErrors(errMsg, round)

                                if ((round == 1) && (Utilities.mixedSourcesNews.isNotEmpty()))
                                {
                                    errMsg = context.resources.getString(com.nissi_miranda.pocketnews.R.string.update_news_error_message)
                                    Toast.makeText(context, errMsg, Toast.LENGTH_LONG).show()
                                }
                            }
                        }
                    },
                    object: Response.ErrorListener {

                        override fun onErrorResponse(error: VolleyError) {

                            Log.e("ServerAccess", "fetchTopHeadlines error -> $error")

                            var errMsg = getErrorMessage(error, context)
                            (context as MainActivity).finishedFetchingTopHeadlinesWithErrors(errMsg, round)

                            if ((round == 1) && (Utilities.mixedSourcesNews.isNotEmpty()))
                            {
                                errMsg = context.resources.getString(com.nissi_miranda.pocketnews.R.string.update_news_error_message)
                                Toast.makeText(context, errMsg, Toast.LENGTH_LONG).show()
                            }
                        }
                    })

            addToVolleyRequestQueue(jsObjRequest)

            return jsObjRequest
        }

        fun fetchTopHeadlinesForSingleSource(context: Context) : JsonObjectRequest
        {
            val url = "https://newsapi.org/v2/top-headlines?sources=${Utilities.openedSourceId}&apiKey=${Constants.newsAPIKey}"

            val jsObjRequest = JsonObjectRequest(Request.Method.GET, url, null,
                    object: Response.Listener<JSONObject> {

                        override fun onResponse(response: JSONObject) {

                            try
                            {
                                if (response.getString("status") != "ok")
                                {
                                    Log.e("ServerAccess", "fetchTopHeadlinesForSingleSource error -> status != ok")
                                    (context as SingleSourceNewsActivity).finishedFetchingNewsWithError()
                                    return
                                }

                                val articles = response.getJSONArray("articles") as JSONArray

                                if (articles.length() == 0)
                                {
                                    Log.e("ServerAccess", "fetchTopHeadlinesForSingleSource error -> number of articles == 0")
                                    (context as SingleSourceNewsActivity).finishedFetchingNewsWithError()
                                    return
                                }

                                (context as SingleSourceNewsActivity).finishedFetchingTopHeadlines(articles)
                            }
                            catch (e: JSONException)
                            {
                                Log.e("ServerAccess", "fetchTopHeadlinesForSingleSource exception -> ${e.localizedMessage}")
                                (context as SingleSourceNewsActivity).finishedFetchingNewsWithError()
                            }
                        }
                    },
                    object: Response.ErrorListener {

                        override fun onErrorResponse(error: VolleyError) {

                            Log.e("ServerAccess", "fetchTopHeadlinesForSingleSource error -> $error")
                            (context as SingleSourceNewsActivity).finishedFetchingNewsWithError()
                        }
                    })

            addToVolleyRequestQueue(jsObjRequest)

            return jsObjRequest
        }

        fun fetchRestOfSingleSourceNews(context: Context) : JsonObjectRequest
        {
            val url = "https://newsapi.org/v2/everything?sources=${Utilities.openedSourceId}&apiKey=${Constants.newsAPIKey}"

            val jsObjRequest = JsonObjectRequest(Request.Method.GET, url, null,
                    object: Response.Listener<JSONObject> {

                        override fun onResponse(response: JSONObject) {

                            try
                            {
                                if (response.getString("status") != "ok")
                                {
                                    Log.e("ServerAccess", "fetchRestOfSingleSourceNews error -> status != ok")
                                    (context as SingleSourceNewsActivity).finishedFetchingNewsWithError()
                                    return
                                }

                                val articles = response.getJSONArray("articles") as JSONArray

                                if (articles.length() == 0)
                                {
                                    Log.e("ServerAccess", "fetchRestOfSingleSourceNews error -> number of articles == 0")
                                    (context as SingleSourceNewsActivity).finishedFetchingNewsWithError()
                                    return
                                }

                                (context as SingleSourceNewsActivity).finishedFetchingRestOfSingleSourceNews(articles)
                            }
                            catch (e: JSONException)
                            {
                                Log.e("ServerAccess", "fetchRestOfSingleSourceNews exception -> ${e.localizedMessage}")
                                (context as SingleSourceNewsActivity).finishedFetchingNewsWithError()
                            }
                        }
                    },
                    object: Response.ErrorListener {

                        override fun onErrorResponse(error: VolleyError) {

                            Log.e("ServerAccess", "fetchRestOfSingleSourceNews error -> $error")
                            (context as SingleSourceNewsActivity).finishedFetchingNewsWithError()
                        }
                    })

            addToVolleyRequestQueue(jsObjRequest)

            return jsObjRequest
        }

        private fun addToVolleyRequestQueue(request: JsonObjectRequest)
        {
            request.tag = App.TAG
            App.instance().volleyRequestQueue()!!.add(request)
        }

        private fun getErrorMessage(error: VolleyError, context: Context) : String
        {
            if (error is NoConnectionError)
            {
                return context.resources.getString(com.nissi_miranda.pocketnews.R.string.volley_no_connection_error_message)
            }
            else if (error is TimeoutError)
            {
                return context.resources.getString(com.nissi_miranda.pocketnews.R.string.volley_timeout_error_message)
            }
            else
            {
                return context.resources.getString(com.nissi_miranda.pocketnews.R.string.general_error_message)
            }
        }
    }
}