package com.nissi_miranda.pocketnews

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONArray
import java.util.*

class MainActivity : BaseActivity()
{
    lateinit private var recyclerView: RecyclerView
    lateinit private var swipeRefresh: SwipeRefreshLayout
    lateinit private var errorView: LinearLayout
    lateinit private var errorMessage: AppCompatTextView
    lateinit private var tryAgainButton: AppCompatButton

    private var timer: Timer? = null
    private var timerTask: TimerTask? = null
    private var topHeadlinesJsonObjectRequest: JsonObjectRequest? = null

    lateinit var layoutManager: GridLayoutManager
    private var scrollPosition = 0

    private var tempNewsDic = HashMap<String, ArrayList<News>>()
    private var tempMixedSourcesNews = ArrayList<News>()

    override fun onCreate(savedInstanceState: Bundle?)
    {
        Utilities.topActivity = "MainActivity"
        super.onCreate(savedInstanceState)
        performLayoutSetup()
    }

    override fun onResume()
    {
        super.onResume()

        if (Utilities.topActivity != "MainActivity")
        {
            Utilities.topActivity = "MainActivity"
            performLayoutSetup()
        }

        if (!Utilities.fetchingTopHeadlines && Utilities.mixedSourcesNews.isEmpty())
        {
            startFetchingTopHeadlines()
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration?)
    {
        super.onConfigurationChanged(newConfig)
        performLayoutSetup()
    }

    override fun onStop()
    {
        super.onStop()

        if (topHeadlinesJsonObjectRequest != null)
        {
            App.instance().volleyRequestQueue()!!.cancelAll(App.TAG)
            topHeadlinesJsonObjectRequest = null
        }

        Utilities.fetchingTopHeadlines = false
        swipeRefresh.isRefreshing = false
    }

    //----------------------------------------------------------------------------------------//
    // Start fetching top headlines
    //----------------------------------------------------------------------------------------//

    private fun startFetchingTopHeadlines()
    {
        if (App.instance().volleyRequestQueue() == null)
        {
            errorMessage.text = resources.getString(com.nissi_miranda.pocketnews.R.string.general_error_message)
            errorView.visibility = View.VISIBLE

            recyclerView.visibility = View.GONE
            swipeRefresh.visibility = View.GONE

            tryAgainButton.setOnClickListener {

                startFetchingTopHeadlines()
            }

            return
        }

        errorView.visibility = View.GONE
        recyclerView.visibility = View.VISIBLE
        swipeRefresh.visibility = View.VISIBLE

        Utilities.fetchingTopHeadlines = true
        swipeRefresh.isRefreshing = true

        val context = this as Context

        timer = Timer()

        timerTask = object: TimerTask() {

            override fun run() {

                timer = null
                timerTask = null
                topHeadlinesJsonObjectRequest = ServerAccess.fetchTopHeadlines(context, 1)
            }
        }

        timer!!.schedule(timerTask!!, 2000)
    }

    //----------------------------------------------------------------------------------------//
    // Call back when finishing fetching top headlines
    //----------------------------------------------------------------------------------------//

    fun finishedFetchingTopHeadlines(articles: JSONArray, round: Int)
    {
        topHeadlinesJsonObjectRequest = null
        setupFetchedTopHeadlines(articles, round)

        if (round == 1) // Finished fetching first round
        {
            topHeadlinesJsonObjectRequest = ServerAccess.fetchTopHeadlines(this, 2)
        }
        else // round == 2 (Finished fetching second round)
        {
            Utilities.fetchingTopHeadlines = false
            swipeRefresh.isRefreshing = false
        }
    }

    //----------------------------------------------------------------------------------------//
    // Call back when finishing fetching top headlines with errors
    //----------------------------------------------------------------------------------------//

    fun finishedFetchingTopHeadlinesWithErrors(errorMsg: String, round: Int)
    {
        if (round == 1) // Finished fetching first round
        {
            topHeadlinesJsonObjectRequest = ServerAccess.fetchTopHeadlines(this, 2)
        }
        else // round == 2 (Finished fetching second round)
        {
            topHeadlinesJsonObjectRequest = null
            setupFetchedTopHeadlines(null, round)
            Utilities.fetchingTopHeadlines = false
            swipeRefresh.isRefreshing = false

            if (Utilities.mixedSourcesNews.isEmpty())
            {
                errorMessage.text = errorMsg
                errorView.visibility = View.VISIBLE

                recyclerView.visibility = View.GONE
                swipeRefresh.visibility = View.GONE

                tryAgainButton.setOnClickListener {

                    startFetchingTopHeadlines()
                }
            }
        }
    }

    //----------------------------------------------------------------------------------------//
    // Setup fetched top headlines
    //----------------------------------------------------------------------------------------//

    private fun setupFetchedTopHeadlines(articles: JSONArray?, round: Int)
    {
        if (articles == null)
        {
            if (tempMixedSourcesNews.size > 0)
            {
                Utilities.newsDic = tempNewsDic
                tempNewsDic = HashMap<String, ArrayList<News>>()
                organizeNews()
                Utilities.mixedSourcesNews = tempMixedSourcesNews
                recyclerView.adapter.notifyDataSetChanged()
                tempMixedSourcesNews = ArrayList<News>()
            }
            else if (tempNewsDic.size > 0) { tempNewsDic = HashMap<String, ArrayList<News>>() }

            return
        }

        val sourceIdsArr = ArrayList<String>()

        for (i in 0..(articles.length() - 1))
        {
            val article = articles.getJSONObject(i)

            if (Utilities.isValidNews(article))
            {
                val sourceId = article.getJSONObject("source").getString("id")
                var title = Utilities.getStringValue(article, "title")
                var description = Utilities.getStringValue(article, "description")
                val url = Utilities.getStringValue(article, "url")
                val urlToImage = Utilities.getArticleImageUrl(article)

                title = Utilities.getParsedHtmlText(title)

                if (description != "")
                {
                    description = Utilities.getParsedHtmlText(description)

                    if (!description.endsWith(".", true))
                    {
                        description += "."
                    }
                }

                val news = News(sourceId, title, description, url, urlToImage, "", "", "", "", "")

                if ((urlToImage != "") && (!sourceIdsArr.contains(sourceId)) && (Constants.sourcesIds.contains(sourceId)))
                {
                    sourceIdsArr.add(sourceId)
                    tempMixedSourcesNews.add(news)
                }

                val arr = tempNewsDic[sourceId] ?: ArrayList<News>()
                var found = false

                if (arr.size > 0)
                {
                    for (n in arr)
                    {
                        if ((n.newsUrl == url) || (n.title == title) || (n.description == description) || (n.newsImageUrl == urlToImage))
                        {
                            found = true
                            break
                        }
                    }
                }

                if (!found)
                {
                    arr.add(news)
                    tempNewsDic[sourceId] = arr
                }
            }
        }

        if (round == 2)
        {
            if (tempMixedSourcesNews.size > 0)
            {
                Utilities.newsDic = tempNewsDic
                tempNewsDic = HashMap<String, ArrayList<News>>()
                organizeNews()
                Utilities.mixedSourcesNews = tempMixedSourcesNews
                recyclerView.adapter.notifyDataSetChanged()
                tempMixedSourcesNews = ArrayList<News>()
            }
            else if (tempNewsDic.size > 0) { tempNewsDic = HashMap<String, ArrayList<News>>() }
        }
    }

    //----------------------------------------------------------------------------------------//
    // Organize news
    //----------------------------------------------------------------------------------------//

    private fun organizeNews()
    {
        Utilities.mixedSourcesNews = tempMixedSourcesNews
        tempMixedSourcesNews = ArrayList<News>()

        for (i in 0..(Constants.sourcesIds.size - 1))
        {
            val srcId = Constants.sourcesIds[i]

            for (y in 0..(Utilities.mixedSourcesNews.size - 1))
            {
                val n = Utilities.mixedSourcesNews[y]

                if (n.sourceId == srcId)
                {
                    tempMixedSourcesNews.add(n)
                }
            }
        }
    }

    //----------------------------------------------------------------------------------------//
    // Perform layout setup
    //----------------------------------------------------------------------------------------//

    private fun performLayoutSetup()
    {
        val isTablet = resources.getBoolean(com.nissi_miranda.pocketnews.R.bool.isTablet)
        val isPortrait = resources.getBoolean(com.nissi_miranda.pocketnews.R.bool.isPortrait)
        val spanCount = if (isTablet) 4 else 2
        val orientation = if (isPortrait) GridLayoutManager.VERTICAL else GridLayoutManager.HORIZONTAL

        if (isPortrait) { setContentView(R.layout.activity_main_portrait) }
        else { setContentView(R.layout.activity_main_landscape) }

        swipeRefresh = findViewById(R.id.swipeRefresh)
        recyclerView = findViewById(R.id.recyclerView)
        errorView = findViewById(R.id.errorView)
        errorMessage = findViewById(R.id.errorMessage)
        tryAgainButton = findViewById(R.id.tryAgainButton)

        swipeRefresh.setOnRefreshListener { startFetchingTopHeadlines() }
        swipeRefresh.isRefreshing = Utilities.fetchingTopHeadlines

        recyclerView.addOnScrollListener(ScrollListener())
        recyclerView.adapter = NewsRecyclerViewAdapter(this)
        layoutManager = GridLayoutManager(this, spanCount, orientation, false)
        layoutManager.spanSizeLookup = CustomSpanSizeLookup()
        recyclerView.layoutManager = layoutManager
        layoutManager.scrollToPosition(scrollPosition)
    }

    private inner class CustomSpanSizeLookup : GridLayoutManager.SpanSizeLookup()
    {
        override fun getSpanSize(position: Int): Int
        {
            return 2
        }
    }

    private inner class ScrollListener : RecyclerView.OnScrollListener()
    {
        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int)
        {
            super.onScrolled(recyclerView, dx, dy)
            scrollPosition = layoutManager.findFirstVisibleItemPosition()
        }
    }
}
