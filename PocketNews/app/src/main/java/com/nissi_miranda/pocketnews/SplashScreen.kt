package com.nissi_miranda.pocketnews

import android.os.Bundle
import android.os.Handler
import android.content.Intent

class SplashScreen : BaseActivity()
{
    private var done = false

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)

        Utilities.topActivity = "SplashScreen"

        done = savedInstanceState?.getBoolean("done") ?: false

        if (!done) { setupHandler() }
    }

    override fun onSaveInstanceState(outState: Bundle?)
    {
        outState?.putBoolean("done", done)
        super.onSaveInstanceState(outState)
    }

    private fun setupHandler()
    {
        done = true

        Handler().postDelayed({

            val preferences = App.instance().getAppPreferences()
            var didFirstLaunch = "No"
            val intent: Intent

            if (preferences != null)
            {
                didFirstLaunch = preferences.getString("FirstLaunch", "No")
            }

            if (didFirstLaunch == "Yes")
            {
                intent = Intent("com.nissi_miranda.pocketnews.MainActivity")
            }
            else
            {
                intent = Intent("com.nissi_miranda.pocketnews.InstructionsActivity")
            }

            startActivity(intent)
            overridePendingTransition(R.anim.slide_from_bottom, R.anim.fade_out)
            finish()

        }, 4000)
    }
}