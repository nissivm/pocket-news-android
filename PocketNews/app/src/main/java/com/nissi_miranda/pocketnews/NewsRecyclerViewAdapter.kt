package com.nissi_miranda.pocketnews

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.support.v4.content.res.ResourcesCompat
import android.view.ViewGroup
import android.view.LayoutInflater
import com.squareup.picasso.Picasso
import android.app.Activity
import android.util.DisplayMetrics
import android.view.View
import android.widget.FrameLayout

class NewsRecyclerViewAdapter(private val context: Context) : RecyclerView.Adapter<NewsCell>()
{
    override fun getItemCount(): Int
    {
        return if (context is MainActivity) Utilities.mixedSourcesNews.size else Utilities.singleSourceNews.size
    }

    override fun getItemViewType(position: Int): Int
    {
        if (context is MainActivity)
        {
            return CellType.SINGLE_NEWS_FULLSCREEN_CELL
        }
        else
        {
            val news = Utilities.singleSourceNews[position]

            if (news.sourceId_n2 != "")
            {
                return CellType.DOUBLE_NEWS_CELL
            }
            else
            {
                if ((position > 0) && (position % 3 == 0) && (news.description != ""))
                {
                    return CellType.SINGLE_NEWS_CELL
                }
                else
                {
                    return CellType.SINGLE_NEWS_FULLSCREEN_CELL
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsCell?
    {
        val inflater = LayoutInflater.from(parent.context)

        if (viewType == CellType.SINGLE_NEWS_CELL)
        {
            val layout = inflater.inflate(R.layout.single_news_cell, parent, false)
            return SingleNewsCell(layout)
        }
        else if (viewType == CellType.SINGLE_NEWS_FULLSCREEN_CELL)
        {
            val layout = inflater.inflate(R.layout.single_news_fullscreen_cell, parent, false)
            return SingleNewsFullscreenCell(layout, context)
        }
        else // DoubleNewsCell
        {
            val layout = inflater.inflate(R.layout.double_news_cell, parent, false)
            return DoubleNewsCell(layout)
        }
    }

    override fun onBindViewHolder(cell: NewsCell, position: Int)
    {
        val news: News
        val actv: Activity
        val arr = getNewsCellFrameSize()
        var width = arr[0]
        var height = arr[1]

        if (cell is SingleNewsCell)
        {
            cell.newsCellFrame.layoutParams.width = arr[0]
            cell.newsCellFrame.layoutParams.height = arr[1]

            news = Utilities.singleSourceNews[position]
            actv = context as SingleSourceNewsActivity

            val isPortrait = actv.resources.getBoolean(com.nissi_miranda.pocketnews.R.bool.isPortrait)
            val isTablet = actv.resources.getBoolean(com.nissi_miranda.pocketnews.R.bool.isTablet)
            val params = cell.container.layoutParams as FrameLayout.LayoutParams

            if (isPortrait)
            {
                if (isTablet)
                {
                    cell.newsDesc.visibility = View.VISIBLE
                    cell.newsDesc.maxLines = 2
                }
                else { cell.newsDesc.maxLines = 3 }

                params.leftMargin = 0
                params.rightMargin = 0
            }
            else
            {
                if (isTablet) { cell.newsDesc.visibility = View.GONE }
                else { cell.newsDesc.maxLines = 1 }

                params.leftMargin = 1
                params.rightMargin = 1
            }

            cell.container.layoutParams = params

            val drawableId = Utilities.getDrawableForSourceId(Utilities.openedSourceId)

            if (!isTablet)
            {
                width /= 2
                height /= 2
            }

            height /= 2

            Picasso.with(context).load(news.newsImageUrl).placeholder(drawableId).error(drawableId)
                    .resize(width, height).onlyScaleDown().centerCrop().into(cell.newsImage)
        }
        else if (cell is SingleNewsFullscreenCell)
        {
            cell.newsCellFrame.layoutParams.width = arr[0]
            cell.newsCellFrame.layoutParams.height = arr[1]

            val drawableId: Int

            if (context is MainActivity)
            {
                news = Utilities.mixedSourcesNews[position]
                actv = context
                drawableId = Utilities.getDrawableForSourceId(news.sourceId)

                cell.sourceName.text = Utilities.getTitleForSourceId(news.sourceId)

                val drawable = ResourcesCompat.getDrawable(context.resources, drawableId, null)
                cell.sourceImage.setImageDrawable(drawable)

                cell.sourceView.setOnClickListener {

                    if (!Utilities.fetchingTopHeadlines)
                    {
                        Utilities.openedSourceId = news.sourceId
                        val intent = Intent("com.nissi_miranda.pocketnews.SingleSourceNewsActivity")
                        actv.startActivity(intent)
                        actv.overridePendingTransition(R.anim.slide_from_right, R.anim.idle_animation)
                    }
                }
            }
            else
            {
                news = Utilities.singleSourceNews[position]
                actv = context as SingleSourceNewsActivity
                drawableId = Utilities.getDrawableForSourceId(Utilities.openedSourceId)
            }

            val isTablet = actv.resources.getBoolean(com.nissi_miranda.pocketnews.R.bool.isTablet)

            if (!isTablet)
            {
                width /= 2
                height /= 2
            }

            Picasso.with(context).load(news.newsImageUrl).placeholder(drawableId).error(drawableId)
                    .resize(width, height).onlyScaleDown().centerCrop().into(cell.newsImage)
        }
        else // DoubleNewsCell
        {
            cell as DoubleNewsCell
            cell.newsCellFrame.layoutParams.width = arr[0]
            cell.newsCellFrame.layoutParams.height = arr[1]

            news = Utilities.singleSourceNews[position]
            actv = context as SingleSourceNewsActivity

            val drawableId = Utilities.getDrawableForSourceId(Utilities.openedSourceId)
            val drawable = ResourcesCompat.getDrawable(context.resources, drawableId, null)
            cell.logoImage.setImageDrawable(drawable)
            cell.logoImage_n2.setImageDrawable(drawable)

            cell.newsInfo_n2.setOnClickListener {

                if (!Utilities.fetchingTopHeadlines && !Utilities.fetchingRestOfSingleSourceNews)
                {
                    val intent = Intent("com.nissi_miranda.pocketnews.WebViewActivity")
                        intent.putExtra("stringUrlToOpen", news.newsUrl_n2)
                    actv.startActivity(intent)
                    actv.overridePendingTransition(R.anim.slide_from_bottom, R.anim.idle_animation)
                }
            }

            cell.newsTitle_n2.text = news.title_n2
            cell.newsDesc_n2.text = news.description_n2

            val isTablet = actv.resources.getBoolean(com.nissi_miranda.pocketnews.R.bool.isTablet)

            if (isTablet)
            {
                val isPortrait = actv.resources.getBoolean(com.nissi_miranda.pocketnews.R.bool.isPortrait)

                if (isPortrait)
                {
                    cell.newsDesc.maxLines = 3
                    cell.newsDesc_n2.maxLines = 3
                }
                else
                {
                    cell.newsDesc.maxLines = 2
                    cell.newsDesc_n2.maxLines = 2
                }
            }

            cell.shareButton_n2.setOnClickListener {

                if (!Utilities.fetchingTopHeadlines && !Utilities.fetchingRestOfSingleSourceNews)
                {
                    Utilities.shareNewsUrl(context, news.newsUrl_n2)
                }
            }
        }

        cell.newsInfo.setOnClickListener {

            if (!Utilities.fetchingTopHeadlines && !Utilities.fetchingRestOfSingleSourceNews)
            {
                val intent = Intent("com.nissi_miranda.pocketnews.WebViewActivity")
                intent.putExtra("stringUrlToOpen", news.newsUrl)
                actv.startActivity(intent)
                actv.overridePendingTransition(R.anim.slide_from_bottom, R.anim.idle_animation)
            }
        }

        cell.newsTitle.text = news.title
        cell.newsDesc.text = news.description

        cell.shareButton.setOnClickListener {

            if (!Utilities.fetchingTopHeadlines && !Utilities.fetchingRestOfSingleSourceNews)
            {
                Utilities.shareNewsUrl(context, news.newsUrl)
            }
        }
    }

    private fun getNewsCellFrameSize() : ArrayList<Int>
    {
        val actv = if (context is MainActivity) context else context as SingleSourceNewsActivity

        val isTablet = actv.resources.getBoolean(com.nissi_miranda.pocketnews.R.bool.isTablet)
        val isPortrait = actv.resources.getBoolean(com.nissi_miranda.pocketnews.R.bool.isPortrait)

        val displayMetrics = DisplayMetrics()
        actv.windowManager.defaultDisplay.getMetrics(displayMetrics)
        val side = if (displayMetrics.widthPixels < displayMetrics.heightPixels) displayMetrics.widthPixels else displayMetrics.heightPixels

        var width = side
        var height = side

        if (!isPortrait)
        {
            height = side - getStatusBarHeight(actv)

            if (actv is SingleSourceNewsActivity)
            {
                val topBarHeight = actv.resources.getDimension(com.nissi_miranda.pocketnews.R.dimen.source_img_size)
                height -= topBarHeight.toInt()
            }
        }

        if (isTablet)
        {
            width /= 2
            height /= 2
        }

        val arr = ArrayList<Int>()
            arr.add(width)
            arr.add(height)

        return arr
    }

    private fun getStatusBarHeight(actv: Activity) : Int
    {
        var result = 0
        val resourceId = actv.resources.getIdentifier("status_bar_height", "dimen", "android")

        if (resourceId > 0)
        {
            result = actv.resources.getDimensionPixelSize(resourceId)
        }

        return result
    }
}