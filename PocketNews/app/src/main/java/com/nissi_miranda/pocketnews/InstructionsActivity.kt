package com.nissi_miranda.pocketnews

import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.instructions_activity.*

class InstructionsActivity : BaseActivity()
{
    override fun onCreate(savedInstanceState: Bundle?)
    {
        Utilities.topActivity = "InstructionsActivity"
        super.onCreate(savedInstanceState)
        setContentView(R.layout.instructions_activity)

        gotItButton.setOnClickListener {

            val preferences = App.instance().getAppPreferences()

            if (preferences != null)
            {
                val editor = preferences.edit()

                if (editor != null)
                {
                    editor.putString("FirstLaunch", "Yes")
                    editor.apply()
                }
            }

            val intent = Intent("com.nissi_miranda.pocketnews.MainActivity")
            startActivity(intent)
            overridePendingTransition(R.anim.slide_from_bottom, R.anim.fade_out)
            finish()
        }
    }
}