package com.nissi_miranda.pocketnews

import android.app.Activity
import android.os.Bundle
import android.view.WindowManager

abstract class BaseActivity : Activity()
{
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        // Changing status bar color:

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)

        if (Utilities.topActivity == "InstructionsActivity")
        {
            window.statusBarColor = resources.getColor(com.nissi_miranda.pocketnews.R.color.Green, null)
        }
        else
        {
            window.statusBarColor = resources.getColor(com.nissi_miranda.pocketnews.R.color.Gray, null)
        }
    }

    override fun onBackPressed()
    {
        if ((Utilities.topActivity == "SplashScreen") ||
            (Utilities.topActivity == "InstructionsActivity") ||
            (Utilities.topActivity == "MainActivity"))
        {
            super.onBackPressed()
        }
        else if (Utilities.topActivity == "SingleSourceNewsActivity")
        {
            finish()
            overridePendingTransition(R.anim.idle_animation, R.anim.slide_to_right)
        }
        else // WebViewActivity
        {
            finish()
            overridePendingTransition(R.anim.idle_animation, R.anim.slide_to_bottom)
        }
    }
}