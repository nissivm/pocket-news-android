package com.nissi_miranda.pocketnews

import android.content.Context
import android.content.Intent
import android.text.Html
import org.json.JSONObject

class Utilities
{
    companion object
    {
        var topActivity = "MainActivity"

        var fetchingTopHeadlines = false
        var fetchingRestOfSingleSourceNews = false

        var newsDic = HashMap<String, ArrayList<News>>()
        var mixedSourcesNews = ArrayList<News>()
        var singleSourceNews = ArrayList<News>()

        lateinit var openedSourceId: String

        fun getSourcesIdsToRequest(round: Int) : String
        {
            var str = ""
            var counter = 0
            var index = if (round == 1) 0 else 14
            val numOfItems = 14

            while (counter < numOfItems)
            {
                str += Constants.sourcesIds[index]
                counter++
                index++

                if (counter < numOfItems)
                {
                    str += ","
                }
            }

            return str
        }

        fun getTitleForSourceId(sourceId : String) : String
        {
            val idx = Constants.sourcesIds.indexOf(sourceId)
            return Constants.sourcesNames[idx]
        }

        fun getDrawableForSourceId(sourceId : String) : Int
        {
            val idx = Constants.sourcesIds.indexOf(sourceId)
            return Constants.sourcesLogos[idx]
        }

        fun isValidNews(article: JSONObject) : Boolean
        {
            val title = Utilities.getStringValue(article, "title")
            val description = Utilities.getStringValue(article, "description")
            val url = Utilities.getStringValue(article, "url")
            val urlToImage = Utilities.getArticleImageUrl(article)

            return ((title != "") && (url != "") && ((description != "") || (urlToImage != "")))
        }

        fun getArticleImageUrl(article: JSONObject) : String
        {
            val urlToImage = Utilities.getStringValue(article, "urlToImage")

            if ((urlToImage != "") && !urlToImage.contains("logo", true) &&
                    !urlToImage.contains("default", true))
            {
                if (!urlToImage.startsWith("http://", true) &&
                        !urlToImage.startsWith("https://", true))
                {
                    return if (urlToImage.startsWith("//", true)) "http:" + urlToImage else "http://" + urlToImage
                }

                return urlToImage
            }

            return ""
        }

        fun getStringValue(article: JSONObject, key: String) : String
        {
            if (article.isNull(key))
            {
                return ""
            }

            return article.getString(key) ?: ""
        }

        @Suppress("deprecation")
        fun getParsedHtmlText(htmlText: String) : String
        {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) //  Android 7.0 - Nougat (API level 24)
            {
                return Html.fromHtml(htmlText, Html.FROM_HTML_MODE_LEGACY).toString()
            }
            else
            {
                return Html.fromHtml(htmlText).toString()
            }
        }

        fun shareNewsUrl(context: Context, newsUrl: String)
        {
            val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.putExtra(Intent.EXTRA_TEXT, newsUrl)
                shareIntent.type = "text/plain"

            context.startActivity(Intent.createChooser(shareIntent, context.resources.getText(R.string.share_news_url)))
        }
    }
}