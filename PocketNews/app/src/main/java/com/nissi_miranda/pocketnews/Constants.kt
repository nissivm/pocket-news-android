package com.nissi_miranda.pocketnews

class Constants
{
    companion object
    {
        val newsAPIKey = "Your key"

        val sourcesIds = arrayOf<String>("fox-news", "abc-news", "cbs-news",
                                         "nbc-news", "msnbc", "newsweek",
                                         "the-new-york-times", "the-washington-post", "daily-mail",
                                         "the-guardian-uk", "the-telegraph", "cbc-news",
                                         "abc-news-au", "the-guardian-au", "the-irish-times",
                                         "globo", "le-monde", "bild",
                                         "la-repubblica", "el-mundo", "la-nacion",
                                         "la-gaceta", "national-geographic", "wired",
                                         "mashable", "fox-sports", "marca",
                                         "entertainment-weekly")

        val sourcesNames = arrayOf<String>("Fox News", "ABC News", "CBS News",
                                           "NBC News", "MSNBC", "Newsweek",
                                           "The New York Times", "The Washington Post", "Daily Mail",
                                           "The Guardian UK", "The Telegraph", "CBC News",
                                           "ABC News AU", "The Guardian AU", "The Irish Times",
                                           "Globo", "Le Monde", "Bild",
                                           "La Repubblica", "El Mundo", "La Nacion",
                                           "La Gaceta", "National Geographic", "Wired",
                                           "Mashable", "Fox Sports", "Marca",
                                           "Entertainment Weekly")

        val sourcesLogos = arrayOf<Int>(R.drawable.fox_news, R.drawable.abc_news, R.drawable.cbs_news,
                                        R.drawable.nbc_news, R.drawable.msnbc, R.drawable.newsweek,
                                        R.drawable.the_new_york_times, R.drawable.the_washington_post, R.drawable.daily_mail,
                                        R.drawable.the_guardian_uk, R.drawable.the_telegraph, R.drawable.cbc_news,
                                        R.drawable.abc_news_au, R.drawable.the_guardian_au, R.drawable.the_irish_times,
                                        R.drawable.globo, R.drawable.le_monde, R.drawable.bild,
                                        R.drawable.la_repubblica, R.drawable.el_mundo, R.drawable.la_nacion,
                                        R.drawable.la_gaceta, R.drawable.national_geographic, R.drawable.wired,
                                        R.drawable.mashable, R.drawable.fox_sports, R.drawable.marca,
                                        R.drawable.entertainment_weekly)
    }
}