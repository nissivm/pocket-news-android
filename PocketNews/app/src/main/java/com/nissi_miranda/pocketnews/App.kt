package com.nissi_miranda.pocketnews

import android.app.Application
import android.content.SharedPreferences
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

class App : Application()
{
    companion object {

        val TAG = "PocketNews"
        lateinit private var instance: App
        private var vRequestQueue: RequestQueue? = null
        private var preferences: SharedPreferences? = null

        fun instance() : App { return instance }
    }

    override fun onCreate()
    {
        super.onCreate()
        instance = this
    }

    fun volleyRequestQueue() : RequestQueue?
    {
        if (vRequestQueue == null)
        {
            vRequestQueue = Volley.newRequestQueue(this)
        }

        return vRequestQueue
    }

    fun getAppPreferences() : SharedPreferences?
    {
        if (preferences == null)
        {
            preferences = getSharedPreferences(TAG, 0)
        }

        return preferences
    }
}